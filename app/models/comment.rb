class Comment < ActiveRecord::Base
  belongs_to :post
  validates_presence_of :post_id
  validates_presence_of :body
  attr_accessible :post_id, :body
end